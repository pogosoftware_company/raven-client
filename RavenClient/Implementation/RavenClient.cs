using System;
using Microsoft.Extensions.Options;
using RavenClient.Extensions;
using RavenClient.Interfaces;
using RavenClient.Models;
using Raven.Client.Documents;
using System.Linq.Expressions;
using Raven.Client.Documents.Conventions;

namespace RavenClient.Implementation
{
    public class RavenClient : IRavenClient
    {
        private static RavenClientSettings _settings;
        private static Lazy<IDocumentStore> _store = new Lazy<IDocumentStore>(CreateDocumentStore);

        public RavenClient(IOptions<RavenClientSettings> optionsAccessor)
        {
            if (optionsAccessor == null) throw new ArgumentNullException(nameof(optionsAccessor));
            
            _settings = optionsAccessor.Value;  
        }

        public IDocumentStore Store => _store.Value;

        public IRavenClient CreateDatabaseIfItDoesNotExist()
        {
            Store.CreateDatabaseIfNotExists(_settings.Database);
            return this;
        }

        public IRavenClient WithConventions<TValue>(Expression<Func<DocumentConventions, TValue>> field, TValue value)
        {
            Store.Conventions.SetPropertyValue<TValue>(field, value);            
            return this;
        }

        public void Initialize()
        {
            Store.Initialize();
        }

        private static IDocumentStore CreateDocumentStore()
        {
            return new DocumentStore
            {
                Urls = new[] { _settings.ServerUrl },
                Database = _settings.Database
            };;
        }
    }
}