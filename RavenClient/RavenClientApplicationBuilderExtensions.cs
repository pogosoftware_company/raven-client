using Microsoft.AspNetCore.Builder;
using RavenClient.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace RavenClient
{
    public static class RavenClientApplicationBuilderExtensions
    {
        public static IRavenClient UseRavenClient(this IApplicationBuilder applicationBuilder)
        {
            return applicationBuilder.ApplicationServices.GetService<IRavenClient>();
        }
    }
}