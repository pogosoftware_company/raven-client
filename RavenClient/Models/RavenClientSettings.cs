namespace RavenClient.Models
{
    public class RavenClientSettings
    {
        public string ServerUrl { get; set; }

        public string Database { get; set; }
    }
}