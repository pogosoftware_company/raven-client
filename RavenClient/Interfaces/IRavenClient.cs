using System;
using System.Linq.Expressions;
using Raven.Client.Documents;
using Raven.Client.Documents.Conventions;

namespace RavenClient.Interfaces
{
    public interface IRavenClient
    {
        IDocumentStore Store { get; }

        IRavenClient CreateDatabaseIfItDoesNotExist();
        IRavenClient WithConventions<TValue>(Expression<Func<DocumentConventions, TValue>> field, TValue value);

        void Initialize();
    }
}